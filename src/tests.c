#include "tests.h"

static void test1()
{
    printf("Тест 1, простая работа\n");
    heap_init(1);

    void *alloc = _malloc(64);

    debug_heap(stdout, HEAP_START);
    _free(alloc);

    munmap(HEAP_START, 8192);
    printf("\nТест успешно пройден\n");
}

static void test2()
{
    printf("Тест 2, освобождение блока памяти\n");

    heap_init(1);

    void *alloc = _malloc(32);
    void *alloc2 = _malloc(64);

    debug_heap(stdout, HEAP_START);
    _free(alloc);

    debug_heap(stdout, HEAP_START);
    _free(alloc2);

    munmap(HEAP_START, 8192);

    printf("\nТест успешно пройден\n");
}

static void test3()
{
    printf("Тест 3, расширение региона памяти\n");

    heap_init(1);

    debug_heap(stdout, HEAP_START);

    void *alloc = _malloc(8192);

    debug_heap(stdout, HEAP_START);
    _free(alloc);

    munmap(HEAP_START, 20480);

    printf("\nТест успешно пройден\n");
}

void run_tests()
{
    test1();
    test2();
    test3();
    printf("Все тесты пройдены успешно.\n");
}